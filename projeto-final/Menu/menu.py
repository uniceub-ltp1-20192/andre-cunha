from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.esportivo import sport

class Menu:
    @staticmethod
    def menuPrincipal():
        print("""
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")
        return Validador.validar("[0-4]",
        """Opcao do menu deve estar entre {}""",
        """Opcao {} Valida!""")


    @staticmethod
    def menuConsultar():
       return input('''
0 - Voltar
1 - Consultar por identificador
2 - Consultar por Propriedade

> ''')

    @staticmethod
    def menuIniciar():
        opMenu = ""
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()

            if opMenu == "1":
                print("entrou em Consultar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if(retorno != None):
                            print(retorno)   
                        else:
                            print("""
    -----------------------
    | Nao foi encontrado  |
    | nenhum registro com |
    | esse identificador  |
    -----------------------
    """)
                        
                    elif opMenu == "2":
                        Menu.menuBuscaPorAtributo(d)

                    elif opMenu == "0":
                        print()
                        print("Voltando")
                opMenu = ""
               

            elif opMenu == "2":
                print("entrou em Inserir")
                Menu.menuInserir(d)


            elif opMenu == "3":
                print("entrou em alterar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)

                    elif opMenu == "2":
                        print("entrou no consultar por Propriedade")
                        retorno = Menu.menuBuscaPorAtributo(d)
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""


            elif opMenu == "4":
                print("entrou em deletar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()

                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)

                    elif opMenu == "2":
                        print("entrou no consultar por Propriedade")
                        retorno = Menu.menuBuscaPorAtributo(d)
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""

            elif opMenu == "0":
                print("Saindo")
            elif opMenu == "":
                print("")
            else:
                print("Informe uma opcao valida!")

    @staticmethod
    def menuInserir(d):   
        teste = sport()
        teste.peso = Validador.validarPeso(teste.peso, "Informe um peso: ")
        print("Ecolha uma das cores: [branco][preto][cinza][vermelho][amarelo]")
        teste.cor = Validador.validarCor(teste.cor, "Informe uma cor: ")
        teste.PtenciaDoMotor = Validador.validarMotor(teste.PtenciaDoMotor, "Informe a potencia do motor <v12/v8>: ")
        print("Escolha uma das marcas: [toyota][ferrari][mercedes][bmw][lexus]")
        teste.marca = Validador.validarMarca(teste.marca, "Informe uma marca: ")
        d.inserirDado(teste)

    @staticmethod
    def menuAlterar(retorno,d):
        print(retorno)
        retorno.peso = Validador.validarPeso(retorno.peso,"Informe um peso: ")
        print("Ecolha uma das cores: [branco][preto][cinza][vermelho][amarelo]")
        retorno.cor = Validador.validarCor(retorno.cor,"Informe uma cor: ")
        retorno.PtenciaDoMotor = Validador.validarMotor(retorno.PtenciaDoMotor,"Informe a potencia do motor <v12/v8>: ")
        print("Escolha uma das marcas: [toyota][ferrari][mercedes][bmw][lexus]")
        retorno.marca = Validador.validarMarca(retorno.marca,"Informa a marca: ")
      
        d.alterarDado(retorno)

        
    def menuDeletar(entidade, d):
        print(entidade)
        resposta = input(
            """Deseja deletar o carro esportivo?
            <s/n>: """)
        if(resposta == "S" or  resposta == "s"):
            d.deletar(entidade)
            print("Registro deletadomenuBuscaPorAtributo...")
        else:
            print("Registro não deletado...")


    @staticmethod
    def menuBuscaPorIdentificador(d):        
        retorno = d.buscarPorIdentificador(Validador.validar(r'\d+','',''))
        return retorno

    @staticmethod
    def menuBuscaPorAtributo(d):
        retorno = d.buscarPorAtributo(input("Informe o peso de um carro: "))

        print(retorno)

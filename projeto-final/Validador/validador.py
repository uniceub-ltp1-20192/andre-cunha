import re

class Validador:
    @staticmethod
    def validar(expReg, msgInvalido, msgValido):
        teste = False
        while teste == False:
            valor = input("Informe um valor: ")
            verificarEx = re.match(expReg, valor)
            
            if verificarEx == None:
                print(msgInvalido.format(expReg))
            else:
                print(msgValido.format(valor))
                return valor

    @staticmethod
    def validarValorEntrada(valorAtual,msg):
        novoValor = input(msg)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorAtual

    @staticmethod
    def validarValorInformado(valorAtual,textoMsg):
        novoValor = input(textoMsg)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorAtual

    @staticmethod
    def validarMotor(pmotor, msg):
        motor1 = "v12"
        motor2 = "v8" 
        tst = False
        
        while tst == False:
            novoMotor = input(msg)
            if novoMotor == None or novoMotor == '':
                return pmotor
            if novoMotor == motor1 or novoMotor == motor2:
                return novoMotor               
            else:
                print("Incorreto! Tente escolher motor 'v12' ou 'v8'.")

    @staticmethod
    def validarPeso(ppeso, msg):
        tsx = False

        while tsx == False:
            novoPeso = input(msg)
            if novoPeso == None or novoPeso == '':
                return ppeso
            if int(novoPeso) >= int(1290) and int(novoPeso) <= int(2500):
                return novoPeso
            else:
                print("Incorreto! O peso do carro deve ser entre 1290 - 2500 kg.")

    @staticmethod
    def validarCor(pcor, msg):
        test = False
        a = 'branco'
        b = 'preto'
        c = 'cinza'
        d = 'vermelho'
        e = 'amarelo'

        while test == False:
            novaCor = input(msg)
            if novaCor == None or novaCor == '':
                return pcor
            if novaCor == a or novaCor == b or novaCor == c or novaCor == d or novaCor == e:
                return novaCor
            else:
                print("Incorreto! Ecolha uma das cores: [branco][preto][cinza][vermelho][amarelo]")

    @staticmethod
    def validarMarca(pmarca, msg):
        tst = False
        z = 'toyota'
        x = 'ferrari'
        y = 'mercedes'
        w = 'bmw'
        v = 'lexus'

        while tst == False:
            novaMarca = input(msg)
            if novaMarca == None or novaMarca == '':
                return  pmarca
            if novaMarca == z or novaMarca == x or novaMarca == y or novaMarca == w or novaMarca == v:
                return novaMarca
            else:
                print("Incorreto! Escolha uma das marcas: [toyota][ferrari][mercedes][bmw][lexus]")
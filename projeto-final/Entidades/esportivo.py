from Entidades.carros import carros

class sport(carros):
  def __init__(self,PtenciaDoMotor = "v12"):
    super().__init__()
    self._PtenciaDoMotor = PtenciaDoMotor

  @property
  def PtenciaDoMotor(self):
    return self._PtenciaDoMotor

  @PtenciaDoMotor.setter
  def PtenciaDoMotor(self,PtenciaDoMotor):
    self._PtenciaDoMotor = PtenciaDoMotor

  def acelerar(self):
    print("VRUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUMMMMMMMMMMMMMMM")
  
  def __str__(self):
    return """
  ------ Esportivo ------
  Identificador: {}
  Marca: {}
  Cor: {}
  Peso: {}
  Potencia do Motor: {}
  -----------------------
  """.format(self.identificador,
    self.marca,
    self.cor,
    self.peso,
    self.PtenciaDoMotor)

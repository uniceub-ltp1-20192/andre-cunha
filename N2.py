#2.1 
onion_num   = 935
onion_price = 3

print ("Você pagara:")
print (onion_num * onion_price)

print ("--------------------------------")
#2.2
cebolas = 300 #declara a variavel cebolas
cebolas_na_caixa = 120 #declara a variavel cebolas_na_caixa
espaco_caixa = 55 #declara a variavel espaco_caixa
caixas = 60 #declara a variavel caixas
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa#o total de cebolas menos as cebolas que estao dentro da caixa da o valor das cebolas que estao de fora da caixa 
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)#as cebolas que estao na caixa dividido pelo espaço que a dentro de cada caixa gera as caixas com cebola menos o total de caixa vai sobrar apenas as caixas vazias
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa#as cebolas que estao de fora da caixa dividia pelo espaço dela mostra quantas caixas ainda sao nescessarias

print ("Existem", cebolas_na_caixa, "cebolas encaixotadas") #vai mostrar a variavel cebolas_na_caixa
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa") #vai mostrar a variavel cebolas_fora_da_caixa
print ("Em cada caixa cabem", espaco_caixa, "cebolas") #vai mostrar a variavel espaco_caixa
print ("Ainda temos,", caixas_vazias, "caixas vazias") #vai mostrar a variavel caixas_vazias
print ("Então, precisamos de", caixas_necessarias, '''caixas para empacotar todas as cebolas''') #vai mostrar a variavel caixas_necessarias
